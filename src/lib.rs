/*
 *  This file is part of trie_lib.
 *
 *  trie_lib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  trie_lib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with trie_lib.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::collections::{HashMap, LinkedList};

#[derive(Debug, Default)]
/// Single node of the Trie tree
struct TrieNode<LabelType: Clone> {
    children: HashMap<char, TrieNode<LabelType>>,
    is_end_of_word: Option<LabelType>,
}

#[derive(Debug, Default)]
/// Trie data structure where is word is associated with a label of type LabelType
///
/// Usage :
///
/// Testing full words
/// ```
/// # #[macro_use] extern crate trie_lib;
/// # use crate::trie_lib::Trie;
/// #
/// #[derive(Clone,PartialEq,Debug)]
/// enum TestLabels {
///     Blah,
///     Chombier,
///     Foo,
///     Bar,
/// }
///
/// use TestLabels::{Blah,Chombier,Foo,Bar};
///
/// # fn main(){
/// let mut trie = Trie::new();
/// trie.insert_word(String::from("blah"),Blah);
/// trie.insert_word(String::from("chombier"),Chombier);
/// trie.insert_word(String::from("foo"),Foo);
/// trie.insert_word(String::from("bar"),Bar);
/// assert_eq!(trie.test_word(&String::from("blah")),Some(Blah));
/// assert_eq!(trie.test_word(&String::from("chombier")),Some(Chombier));
/// assert_eq!(trie.test_word(&String::from("bar")),Some(Bar));
/// assert_eq!(trie.test_word(&String::from("bloh")),None);
/// assert_eq!(trie.test_word(&String::from("")),None);
/// # }
/// ```
///
/// Testing one character at a time
/// ```
/// # #[macro_use] extern crate trie_lib;
/// # use crate::trie_lib::Trie;
/// #
/// # #[derive(Clone,PartialEq,Debug)]
/// # enum TestLabels {
/// #     Blah,
/// #     Chombier,
/// #     Foo,
/// #     Bar,
/// # }
/// #
/// # use TestLabels::{Blah,Chombier,Foo,Bar};
/// #
/// # fn main(){
/// # let mut trie = Trie::new();
/// # trie.insert_word(String::from("blah"),Blah);
/// # trie.insert_word(String::from("chombier"),Chombier);
/// let mut iter = trie.get_iter();
///
/// assert_eq!(iter.next('b'),true);
/// assert_eq!(iter.next('l'),true);
/// assert_eq!(iter.next('a'),true);
/// assert_eq!(iter.next('h'),true);
/// assert_eq!(iter.is_end(),Some(Blah));
///
/// iter = trie.get_iter();
/// assert_eq!(iter.next('b'),true);
/// assert_eq!(iter.next('l'),true);
/// assert_eq!(iter.next('o'),false);
///
/// iter = trie.get_iter();
/// assert_eq!(iter.next('o'),false);
///
/// iter = trie.get_iter();
/// assert_eq!(iter.is_end(),None);
/// # }
/// ```
pub struct Trie<LabelType: Clone> {
    master_node: TrieNode<LabelType>,
}

#[derive(Debug)]
/// "Iterator" allowing to use the trie in an optimised way by inputing each char seprately
///
/// Usage :
/// ```
/// # #[macro_use] extern crate trie_lib;
/// # use crate::trie_lib::Trie;
/// #
/// #[derive(Clone,PartialEq,Debug)]
/// enum TestLabels {
///     Blah,
///     Chombier,
///     Foo,
///     Bar,
/// }
///
/// use TestLabels::{Blah,Chombier,Foo,Bar};
///
/// # fn main(){
/// let mut trie = Trie::new();
/// trie.insert_word(String::from("blah"),Blah);
/// trie.insert_word(String::from("chombier"),Chombier);
/// let mut iter = trie.get_iter();
///
/// assert_eq!(iter.next('b'),true);
/// assert_eq!(iter.next('l'),true);
/// assert_eq!(iter.next('a'),true);
/// assert_eq!(iter.next('h'),true);
/// assert_eq!(iter.is_end(),Some(Blah));
///
/// iter = trie.get_iter();
/// assert_eq!(iter.next('b'),true);
/// assert_eq!(iter.next('l'),true);
/// assert_eq!(iter.next('o'),false);
///
/// iter = trie.get_iter();
/// assert_eq!(iter.next('o'),false);
///
/// iter = trie.get_iter();
/// assert_eq!(iter.is_end(),None);
/// # }
/// ```
///
/// Note that when iter returns false, it stays in the same state as it was before :
///
/// ```
/// # #[macro_use] extern crate trie_lib;
/// # use crate::trie_lib::Trie;
/// #
/// #[derive(Clone,PartialEq,Debug)]
/// # enum TestLabels {
/// #     Blah,
/// #     Chombier,
/// #     Foo,
/// #     Bar,
/// # }
///
/// # use TestLabels::{Blah,Chombier,Foo,Bar};
///
/// # fn main(){
/// let mut trie = Trie::new();
/// trie.insert_word(String::from("blah"),Blah);
/// trie.insert_word(String::from("chombier"),Chombier);
/// let mut iter = trie.get_iter();
///
/// iter = trie.get_iter();
/// assert_eq!(iter.next('b'),true);
/// assert_eq!(iter.next('l'),true);
/// assert_eq!(iter.next('o'),false);
/// assert_eq!(iter.next('a'),true);
/// assert_eq!(iter.next('h'),true);
/// assert_eq!(iter.is_end(),Some(Blah));
/// # }
/// ```
///
/// Getting all possible completions:
/// ```
/// # #[macro_use] extern crate trie_lib;
/// # use crate::trie_lib::Trie;
/// #
/// # #[derive(Clone,PartialEq,Debug)]
/// # enum TestLabels {
/// #     Blah,
/// #     Chombier,
/// #     Foo,
/// #     Bar,
/// # }
/// #
/// # use TestLabels::{Blah,Chombier,Foo,Bar};
/// #
/// # fn main(){
/// let mut trie = Trie::new();
/// trie.insert_word(String::from("blah"),Blah);
/// trie.insert_word(String::from("chombier"),Chombier);
/// trie.insert_word(String::from("foo"),Foo);
/// trie.insert_word(String::from("bar"),Bar);
///
/// let mut iter = trie.get_iter();
/// assert_eq!(iter.next('b'),true);
///
/// let possible_list = iter.get_possible_words();
/// let possible: Vec<&(String,TestLabels)> = possible_list.iter().collect();
///
/// assert_eq!(possible.len(), 2);
/// assert!(possible[0]== &(String::from("lah"),Blah)|| possible[1]== &(String::from("lah"),Blah));
/// assert!(possible[0]== &(String::from("ar"),Bar) || possible[1]== &(String::from("ar"),Bar));
/// # }
pub struct TrieIter<'a, LabelType: Clone> {
    current_node: &'a TrieNode<LabelType>,
}

impl<LabelType: Clone> TrieNode<LabelType> {
    /// Generate a new node
    fn new() -> TrieNode<LabelType> {
        TrieNode {
            children: HashMap::new(),
            is_end_of_word: None,
        }
    }
    /// Insert a word to the current node (works recursively)
    ///
    /// Complexity : O(n) where n is the length of chars
    fn insert_word(&mut self, mut chars: std::str::Chars, end: LabelType) {
        match chars.next() {
            None => self.is_end_of_word = Some(end),
            Some(cur_char) => {
                match self.children.get_mut(&cur_char) {
                    None => {
                        let mut future_child = TrieNode::new();
                        future_child.insert_word(chars, end);
                        self.children.insert(cur_char, future_child);
                    }
                    Some(child) => child.insert_word(chars, end),
                };
            }
        };
    }

    /// Get the child node for on char
    fn get_child(&self, c: char) -> Option<&TrieNode<LabelType>> {
        self.children.get(&c)
    }

    /// Get the words that come from the children of the node and append to them the given prefix
    ///
    /// Complexity : O(n*m) where n is the number of words in the output and m the longuest of them
    fn get_possible_words_with_prefix(&self, prefix: String) -> LinkedList<(String, LabelType)> {
        let mut result = LinkedList::new();
        for (c, node) in self.children.iter() {
            let mut temp = prefix.clone();
            temp.push(*c);
            result.append(&mut node.get_possible_words_with_prefix(temp));
        }

        if let Some(end) = &self.is_end_of_word {
            result.push_back((prefix, end.clone()));
        }
        result
    }
}

impl<LabelType: Clone> Trie<LabelType> {
    /// Generate a new empty Trie structure
    pub fn new() -> Trie<LabelType> {
        Trie {
            master_node: TrieNode::new(),
        }
    }

    /// Insert a word in the trie structure
    ///
    /// Complexity : O(n) where n is the length of s
    pub fn insert_word(&mut self, s: String, end: LabelType) {
        self.master_node.insert_word(s.chars(), end);
    }

    /// Test if a word has been inserted into the trie
    ///
    /// Complexity : O(n) where n is the length of s
    pub fn test_word(&self, s: &str) -> Option<LabelType> {
        let chars = s.chars();
        let mut iter = self.get_iter();
        for c in chars {
            if iter.next(c) {
                continue;
            } else {
                return None;
            };
        }
        iter.is_end()
    }

    /// Returns an iterator to test if a word has been inserted into the trie one character at a time
    pub fn get_iter(&self) -> TrieIter<LabelType> {
        TrieIter {
            current_node: &self.master_node,
        }
    }
}

impl<'a, LabelType: Clone> TrieIter<'a, LabelType> {
    /// Test if a character is still part of the word
    /// None represents the end of a word
    ///
    /// Complexity : O(1)
    pub fn next(&mut self, c: char) -> bool {
        match self.current_node.get_child(c) {
            Some(new_node) => {
                self.current_node = new_node;
                true
            }
            None => false,
        }
    }

    /// Test if the iterator is at the end of the word
    ///
    /// If true, returns it's label
    pub fn is_end(&self) -> Option<LabelType> {
        self.current_node.is_end_of_word.clone()
    }

    /// Get all the possible words from the iterator
    ///
    /// The Output is in a LinkedList with all the possible completions.
    /// The order of the result doesn't mean anything
    ///
    /// Complexity : O(n*m) where n is the number of words in the output and m the longuest of them
    pub fn get_possible_words(&self) -> LinkedList<(String, LabelType)> {
        self.current_node
            .get_possible_words_with_prefix(String::new())
    }

    /// Get all the possible words from the iterator and appends them to the prefix.
    /// This exists because the iterator doesn't save the current prefix.
    ///
    /// Complexity : O(n*m) where n is the number of words in the output and m the longuest of them
    pub fn get_possible_words_add_prefix(&self, prefix: String) -> LinkedList<(String, LabelType)> {
        self.current_node.get_possible_words_with_prefix(prefix)
    }
}

#[cfg(test)]
mod test_trie {
    use super::*;

    #[derive(Clone, PartialEq, Debug)]
    enum TestLabels {
        Blah,
        Chombier,
        Foo,
        Bar,
    }

    use TestLabels::{Bar, Blah, Chombier, Foo};

    #[test]
    fn iterator() {
        let mut trie = Trie::new();
        trie.insert_word(String::from("blah"), Blah);
        trie.insert_word(String::from("chombier"), Chombier);
        let mut iter = trie.get_iter();

        assert_eq!(iter.next('b'), true);
        assert_eq!(iter.next('l'), true);
        assert_eq!(iter.next('a'), true);
        assert_eq!(iter.next('h'), true);
        assert_eq!(iter.is_end(), Some(Blah));

        iter = trie.get_iter();
        assert_eq!(iter.next('c'), true);
        assert_eq!(iter.next('h'), true);
        assert_eq!(iter.next('o'), true);
        assert_eq!(iter.next('m'), true);
        assert_eq!(iter.next('b'), true);
        assert_eq!(iter.next('i'), true);
        assert_eq!(iter.next('e'), true);
        assert_eq!(iter.next('r'), true);
        assert_eq!(iter.is_end(), Some(Chombier));

        iter = trie.get_iter();
        assert_eq!(iter.next('b'), true);
        assert_eq!(iter.next('l'), true);
        assert_eq!(iter.next('o'), false);

        iter = trie.get_iter();
        assert_eq!(iter.next('o'), false);

        iter = trie.get_iter();
        assert_eq!(iter.is_end(), None);
    }

    #[test]
    fn full_word() {
        let mut trie = Trie::new();
        trie.insert_word(String::from("blah"), Blah);
        trie.insert_word(String::from("chombier"), Chombier);
        trie.insert_word(String::from("foo"), Foo);
        trie.insert_word(String::from("bar"), Bar);
        assert_eq!(trie.test_word(&String::from("blah")), Some(Blah));
        assert_eq!(trie.test_word(&String::from("chombier")), Some(Chombier));
        assert_eq!(trie.test_word(&String::from("foo")), Some(Foo));
        assert_eq!(trie.test_word(&String::from("bar")), Some(Bar));
        assert_eq!(trie.test_word(&String::from("bloh")), None);
        assert_eq!(trie.test_word(&String::from("")), None);
        assert_eq!(trie.test_word(&String::from("cha")), None);
        assert_eq!(trie.test_word(&String::from("chombiera")), None);
    }

    #[test]
    fn get_possible_word() {
        let mut trie = Trie::new();
        trie.insert_word(String::from("blah"), Blah);
        trie.insert_word(String::from("chombier"), Chombier);
        trie.insert_word(String::from("foo"), Foo);
        trie.insert_word(String::from("bar"), Bar);
        let mut iter = trie.get_iter();
        assert_eq!(iter.next('b'), true);
        let possible_list = iter.get_possible_words();
        let possible: Vec<&(String, TestLabels)> = possible_list.iter().collect();
        assert_eq!(possible.len(), 2);
        assert!(
            possible[0] == &(String::from("lah"), Blah)
                || possible[1] == &(String::from("lah"), Blah)
        );
        assert!(
            possible[0] == &(String::from("ar"), Bar) || possible[1] == &(String::from("ar"), Bar)
        );

        iter = trie.get_iter();
        assert_eq!(iter.next('f'), true);
        let possible_list = iter.get_possible_words();
        let possible: Vec<&(String, TestLabels)> = possible_list.iter().collect();
        assert_eq!(possible.len(), 1);
        assert!(possible[0] == &(String::from("oo"), Foo));
    }

    #[test]
    fn get_possible_words_add_prefix() {
        let mut trie = Trie::new();
        trie.insert_word(String::from("blah"), Blah);
        trie.insert_word(String::from("chombier"), Chombier);
        trie.insert_word(String::from("foo"), Foo);
        trie.insert_word(String::from("bar"), Bar);
        let mut iter = trie.get_iter();
        assert_eq!(iter.next('b'), true);
        let possible_list = iter.get_possible_words_add_prefix(String::from("b"));
        let possible: Vec<&(String, TestLabels)> = possible_list.iter().collect();
        assert_eq!(possible.len(), 2);
        assert!(
            possible[0] == &(String::from("blah"), Blah)
                || possible[1] == &(String::from("blah"), Blah)
        );
        assert!(
            possible[0] == &(String::from("bar"), Bar)
                || possible[1] == &(String::from("bar"), Bar)
        );

        iter = trie.get_iter();
        assert_eq!(iter.next('f'), true);
        let possible_list = iter.get_possible_words_add_prefix(String::from("f"));
        let possible: Vec<&(String, TestLabels)> = possible_list.iter().collect();
        assert_eq!(possible.len(), 1);
        assert!(possible[0] == &(String::from("foo"), Foo));
    }
}
